'''
This pyhton script reading the HTML and extracting the only table data in  this file.
 After extracting the data we conver that data into JSON format ;
'''
import json;
import re;
from prettytable import PrettyTable
#heading is thee variiable containg the heading of the table in html;
heading=[];
# here opening the file & read the file line  by line
file=open("/home/dev/Downloads/Jay/project-3/home.html","r");
json_file=open("/home/dev/Downloads/Jay/project-3/json_data.json","w+");
line=file.next();
'''
we  are reading the file until the line containg the "<table" and we are matching with regular expression.
when we get tha starting line in whic "<table"  string occur form that line onwars we start reding the data
 until closing table tag does not occur"</table"
'''
while("</html>" not in line):
	if "<table"  in line:
		break;
	line=file.next();

row=1;
col=0;
count=0;
line=file.next();
# json_data is the list of json object  for each  train details;
json_data=[]
#this loop reads the html  file until the closing table tag does not occur
while("</table" not in line):
	'''
	Here we use row =1 because the first row of the table is heading & here we extracting the headding & put those
	heading in the heading list we declae above;
	'''
	if row ==1:
		'''
		if closing row tag occur first time than after this row data is present in next <tr> tag so we set row=2
		so for the next row this  will not execute
		'''
		if "</tr>"  in line:
			row=2;
		if "<td>" in line and "</td>" in line:
				index1=0;
				index2=0;
				index1=re.search("<td>",line).start()+4;
				index2=re.search("</td>",line).start();
				#here we are appending the heading
				heading.append(line[index1:index2]);
		line=file.next();
	'''
	Here we use row =2 because after the first row of the table is not heading & after first row
	we data only 
	'''
	if row==2:
		#data is dictionary object to to put the extracted data 
		data={};
		#here we start reading the row
		if "<tr" in line :
			count =0;
			line=file.next();
		'''
		here we iterate the row until we get the closing row tag
		and for each coloumn we extract the values in tis loop
		'''
		while("</tr>" not in line):
			#print line;
			index1=0;
			index2=0;
			index1=re.search("<td>",line).start()+4;
			index2=re.search("</td>",line).start();
			#here put the coloumn value to its corrosponding heding we initailize before  
			data[heading[count]]= line[index1:index2]
			count+=1;
			line=file.next();

		line=file.next();

		#here we putting the extracting data into  json format
		json_data.append(data);
		
	
file.close();


# here we are writing the data in json format in the file name  json_data.json
with open('json_data.json', 'w+') as fp:
    json.dump(json_data, fp);

# here we are reading the data from the file name  json_data.json
with open('json_data.json', 'r+') as fp:
	json_data=json.load(fp);
	

#printing the json objects in the table foramt
x = PrettyTable();
x.field_names=heading;
for each_json in json_data:
	temp=[];
	for key,value in each_json.items():
		temp.append(value);
	x.add_row(temp);
print(x);

	
	
